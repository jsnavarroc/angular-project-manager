import { AngualeProyectManagerPage } from './app.po';

describe('angular-proyect-manager App', () => {
  let page: AngualeProyectManagerPage;

  beforeEach(() => {
    page = new AngualeProyectManagerPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
