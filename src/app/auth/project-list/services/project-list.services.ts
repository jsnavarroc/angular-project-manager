import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Project} from '../models/project.model';
import {AuthenticationService} from '../../../common/services/authentication.service';
import {HttpService} from '../../../common/services/http.service'; //importo la class con el mismo nombre

@Injectable()
export class ProjectListService extends HttpService{

  projects : Array<Project> = [];//lista de objetos
  constructor(public _http: Http, public _authService: AuthenticationService ){
  super(_http);
  }
  
  getAll(): Observable <Array <Project>>{
    const token = this._authService.user.api_token;
    const apiBaseURL =`${this.apiBaseURL}/projects`;
    return this.get(apiBaseURL, token);
  }

  deleteProject( project:Project ){
    const url =`${this.apiBaseURL}/projects/${project.id}`;
    const token = this._authService.user.api_token;
    //const url ='http://172.104.91.187/projects'
    return this.delete(url, token);
  }
}
