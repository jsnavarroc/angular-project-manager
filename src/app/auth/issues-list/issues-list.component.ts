import { Component, OnInit } from '@angular/core';
import {IssuesListService} from "./services/issues-list.services";
import {Issues} from './models/issues.model';

@Component({
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.css']
})
export class IssuesListComponent implements OnInit {
  isLoading = true;
  issues: Array<Issues>;
  constructor(private  _issuesListServices: IssuesListService) { }

  ngOnInit() {
    this.getAllIssues();
  }
  getAllIssues(){
    this._issuesListServices.getAll().subscribe(
      (data: Issues[])=>{
        //next
        this.issues=data;
        this.isLoading=false;
      },
      err =>{
        console.error(err);
      },
      ()=>{
        console.log('Finished!!');
      }
    
    );
  }
  
  
  onDeleteIssues(issues : Issues){
    //http://localhost:8085/projects/1
    this._issuesListServices.deleteIssues(issues).subscribe((data)=>{
      console.log(data);
      this.getAllIssues();
    });
  }
  
  public setData(sortedIssues) {
    console.log('sortedData: %o', sortedIssues);
    this.issues = sortedIssues;
  }

}
