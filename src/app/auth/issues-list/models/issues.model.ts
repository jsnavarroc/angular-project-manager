export class Issues {
    id?: number;
    title: string;
    slug: string;
    description:string;
    project_id:string;
    reporter:string;
    assignee:string;
    type:string;
    status: string;
    priority: string;
    created_at?: string;
    updated_at?:string;
}

