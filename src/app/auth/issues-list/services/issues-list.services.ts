import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Issues} from '../models/issues.model';
import {AuthenticationService} from '../../../common/services/authentication.service';
import {HttpService} from '../../../common/services/http.service'; //importo la class con el mismo nombre

@Injectable()
export class IssuesListService extends HttpService{
  issues: Array<Issues> = [];//lista de objetos
  constructor(public _http: Http, public _authService: AuthenticationService ){
    super(_http);
  }
  
  
  getAll(): Observable <Array <Issues>>{
    const token = this._authService.user.api_token;
    const apiBaseURL =`${this.apiBaseURL}/issues`;
    return this.get(apiBaseURL, token);
  }
  deleteIssues( issues:Issues ){
    //const url ='http://172.104.91.187/projects'
    const url =`${this.apiBaseURL}/projects/${issues.id}`;
    const token = this._authService.user.api_token;

    return this.delete(url, token);
  }
  /*createIssues( issues:Issues ){
    const url =`http://localhost:8085/issues/${issues.id}`;
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({
      headers:headers
    });
    return this._http.post(url, options).map((response)=>{ //una funcion anonima
      return response.json();
    });
  }*/
}
